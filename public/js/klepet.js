// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};

var preimenovalnaTabela = [
  ["vzdevek","nadimek","id"]
  ];

Klepet.prototype.preveriIme = function(uporabnik,id)
{
  for(var j = 1;j < preimenovalnaTabela.length;j++)
    {
      //console.log(j + ":  :" + preimenovalnaTabela[j][0] + "::" +  preimenovalnaTabela[j][1] + "::" +  preimenovalnaTabela[j][2] + "::"+ uporabnik + "::");
      if(preimenovalnaTabela[j][0] == uporabnik && preimenovalnaTabela[j][2] == -1)
      {
        //uporabnik = preimenovalnaTabela[j][1] + "(" + uporabnik + ")";
        preimenovalnaTabela[j][2] = id;
      }
    }
    for(var j = preimenovalnaTabela.length-1;j > 0 ;j--)
    {
      //console.log(j + ":  :" + preimenovalnaTabela[j][0] + "::" +  preimenovalnaTabela[j][1] + "::" +  preimenovalnaTabela[j][2] + "::"+ uporabnik + "::");
      if(preimenovalnaTabela[j][2] == id)
      {
        preimenovalnaTabela[j][0] = uporabnik;
        uporabnik = preimenovalnaTabela[j][1] + "(" + uporabnik + ")";
        j = 0;
      }
    }
    return uporabnik
}

function preveriImeZasebno(uporabnik)
{
  console.log("dafuq ::" + preimenovalnaTabela.length);
  for(var i = 1;i < preimenovalnaTabela.length;i++)
  {
    console.log(preimenovalnaTabela[i][1] + "::" + uporabnik)
    if(preimenovalnaTabela[i][1] == uporabnik)
    {
      return preimenovalnaTabela[i][0];
    }
    else if(preimenovalnaTabela[i][1] + "(" + preimenovalnaTabela[i][0] + ")" == uporabnik)
    {
      return preimenovalnaTabela[i][0];
    }
  }
  return uporabnik;
}

Klepet.prototype.spremeniIme = function(staroIme,novoIme)   //legacy funkcija, neuporabno
{
  for(var j = 1;j < preimenovalnaTabela.length;j++)
    {
      console.log(j + "::" + preimenovalnaTabela[j][0] + "::" + staroIme);
      if(preimenovalnaTabela[j][0] == staroIme)
      {
        novoIme = novoIme.substr(0,novoIme.length-1);
        preimenovalnaTabela[j][0] = novoIme;
        console.log(novoIme + "--" + preimenovalnaTabela[j][0]);
      }
    }
}

Klepet.prototype.zamenjajIme = function(besedilo)
{
  for(var j = 1;j < preimenovalnaTabela.length;j++)
    {
      //console.log(j + "::" + preimenovalnaTabela[j][0] + "::" + staroIme);
      if(besedilo.indexOf(preimenovalnaTabela[j][0]) == 0)
      {
        besedilo = preimenovalnaTabela[j][1] + "(" + preimenovalnaTabela[j][0] + ")" + besedilo.substr(preimenovalnaTabela[j][0].length,besedilo.length);
      }
    }
    return besedilo;
}

/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  //console.log(ukaz);
  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if (parametri) {
        parametri[1] = preveriImeZasebno(parametri[1]);
        this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
        sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3];
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    case 'barva':
      var barva = besede[1];
      //console.log(barva);
      $('#sporocila').css("background-color", barva);
      $('#kanal').css("background-color", barva);
      break;
    case 'preimenuj':
      var parametri = besede.join('').split('\"');
      //console.log(parametri);
      //console.log(ukaz);
      if(parametri){
        preimenovalnaTabela.push([parametri[1],parametri[3],-1]);
        //console.log(preimenovalnaTabela);
        sporocilo = 'Preimenovanje uspešno';
      }
      else{
        sporocilo = 'Neznan ukaz';
      }
      
      break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};